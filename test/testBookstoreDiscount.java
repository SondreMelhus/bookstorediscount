
import org.junit.jupiter.api.Test;

import java.util.ArrayList;


import static org.junit.jupiter.api.Assertions.*;

class testBookstoreDiscount {

    @Test
    public void discountPriceForTwoBooks_checksThePriceForTwoUniqueBooks_shouldReturnDiscountedPrice() {

        //Assign
        Bookstore bookstore = new Bookstore();
        ArrayList<Integer> books = new ArrayList();
        books.add(0);
        books.add(1);
        double expected = 15.2;

        //Act
        double actual = bookstore.price(books);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void discountPriceForThreeUniqueBooks_checksThePriceForThreeUniqueBooks_shouldReturnDiscountedPrice() {

        //Assign
        Bookstore bookstore = new Bookstore();
        ArrayList<Integer> books = new ArrayList();
        books.add(0);
        books.add(2);
        books.add(3);
        double expected = 21.6;

        //Act
        double actual = bookstore.price(books);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void discountPriceForAllFourBooks_checksThePriceForForAllFourBooks_shouldReturnDiscountedPrice() {

        //Assign
        Bookstore bookstore = new Bookstore();
        ArrayList<Integer> books = new ArrayList();
        books.add(0);
        books.add(1);
        books.add(2);
        books.add(3);
        double expected = 25.6;

        //Act
        double actual = bookstore.price(books);

        //Assert
        assertEquals(expected, actual);
    }
}
