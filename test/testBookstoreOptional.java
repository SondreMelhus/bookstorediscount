import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class testBookstoreOptional {

    @Test
    public void discountPriceForTwoBooksOptional_checksThePriceForTwoUniqueBooks_shouldReturnDiscountedPrice() {

        //Assign
        Bookstore bookstore = new Bookstore();
        ArrayList<Integer> books = new ArrayList();
        books.add(0);
        books.add(0);
        books.add(1);
        double expected = (8.0 + (8 * 2 * 0.95));

        //Act
        double actual = bookstore.price(books);

        //Assert
        assertEquals(expected, actual);
    }
}
