
import org.junit.jupiter.api.Test;

import java.util.ArrayList;


import static org.junit.jupiter.api.Assertions.*;

class testBookstoreBasic {

    @Test
    public void priceForNoBooks_checksThePriceForNoBooks_shouldReturn0() {

        //Assign
        Bookstore bookstore = new Bookstore();
        ArrayList<Integer> books = new ArrayList();
        double expected = 0;

        //Act
        double actual = bookstore.price(books);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void priceForBookOne_checksThePriceForBookOne_shouldReturn8() {

        //Assign
        Bookstore bookstore = new Bookstore();
        ArrayList<Integer> books = new ArrayList();
        books.add(1);
        double expected = 8;

        //Act
        double actual = bookstore.price(books);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void priceForBookTwo_checksThePriceForBookTwo_shouldReturn8() {

        //Assign
        Bookstore bookstore = new Bookstore();
        ArrayList<Integer> books = new ArrayList();
        books.add(2);
        double expected = 8;

        //Act
        double actual = bookstore.price(books);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void priceForBookThree_checksThePriceForBookThree_shouldReturn8() {

        //Assign
        Bookstore bookstore = new Bookstore();
        ArrayList<Integer> books = new ArrayList();
        books.add(3);
        double expected = 8;

        //Act
        double actual = bookstore.price(books);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void priceForThreeCopiesOfBookOne_checksThePriceForThreeCopiesOfBookOne_shouldReturn24() {

        //Assign
        Bookstore bookstore = new Bookstore();
        ArrayList<Integer> books = new ArrayList();
        books.add(1);
        books.add(1);
        books.add(1);
        double expected = 24;

        //Act
        double actual = bookstore.price(books);

        //Assert
        assertEquals(expected, actual);
    }
}
