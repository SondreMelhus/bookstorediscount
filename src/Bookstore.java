import java.util.ArrayList;

public class Bookstore {
    public double price(ArrayList<Integer> books) {
        double price = 8 * books.size();
        double discount = 1;
        long numberOfUniqueBooks = books.stream().distinct().count() - 1;

        if (numberOfUniqueBooks == 3){
             discount = 0.8;
        }
        if (numberOfUniqueBooks == 2){
            discount = 0.9;
        }
        if (numberOfUniqueBooks == 1) {
            discount = 0.95;
        }
        return price * discount;
    }

}
